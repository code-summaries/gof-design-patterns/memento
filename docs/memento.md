<div align="center">
  <h1>Memento</h1>
</div>

<div align="center">
  <img src="memento_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Memento is a behavioral pattern for saving and loading an object's state without revealing its
implementation.**

### Real-World Analogy

_A museum's conservation process._

The museum's team (caretaker) store snapshots (memento) of the artifact's  (originator) state at various points in time.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

Memento (SolverState)- stores internal state of the Originator object. The memento may store as much or as little of the
originator's internal state as necessary at its originator's discretion.- protects against access by objects other than
the originator. Mementos have effectively two interfaces. Caretaker sees a narrow interface to the Memento—it can only
pass the memento to other objects. Originator, in contrast, sees a wide interface, one that lets it access all the data
necessary to restore itself to its previous state. Ideally, only the originator that produced the memento would be
permitted to access the memento's internal state. • Originator (ConstraintSolver)- creates a memento containing a
snapshot of its current internal state.- uses the memento to restore its internal state. • Caretaker (undo mechanism)-
is responsible for the memento's safekeeping.- never operates on or examines the contents of a memento.

### Collaborations

...
A caretaker requests a memento from an originator, holds it for a time, and
passes it back to the originator, as the following interaction diagram illustrates:

ometimes the caretaker won't pass the memento back to the originator, because the originator might never need to revert
to an earlier state.
• Mementos are passive. Only the originator that created a mementowill assign
or retrieve its state

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

Use the Memento pattern when • a snapshot of (some portion of) an object's state must be saved so that it can be
restored to that state later, and • a direct interface to obtaining the state would expose implementation details and
break the object's encapsulation.

### Motivation

- ...

Sometimes it's necessary to record the internal state of an object. This is required
when implementing checkpoints and undo mechanisms that let users back outof
tentative operations orrecoverfrom errors. You must save state information somewhere so that you can restore objects
to their previous states.But objects normally
encapsulate some or all of their state, making it inaccessible to other objects and
impossible to save externally. Exposing this state would violate encapsulation,
which can compromise the application's reliability and extensibility.

Consider for example a graphical editor that supports connectivity between objects. A user can connect two rectangles
with a line, and the rectangles stay connected when the user moves either of them. The editor ensures that the line
stretches to maintain the connection.

A well-known way to maintain connectivity relationships between objectsis with
a constraint-solving system.Wecan encapsulate thisfunctionality in a ConstraintSolver object. ConstraintSolver records
connections as they are made and generates mathematical equations that describe them. It solves these equations
whenever the user makes a connection or otherwise modifies the diagram. ConstraintSolver uses the results of its
calculations to rearrange the graphics so that they
maintain the proper connections.

Supporting undo in this application isn't as easy as it may seem. An obvious way
to undo a move operation is to store the original distance moved and move the object back an equivalent distance.
However, this does not guarantee all objects
will appear where they did before. Suppose there is some slack in the connection. In that case, simply moving the
rectangle back to its original location won't
necessarily achieve the desired effect.

In general, the ConstraintSolver's public interface might be insufficient to allow
precise reversal of its effects on other objects. The undo mechanism must work
more closely with ConstraintSolver to reestablish previous state, but we should
also avoid exposing the ConstraintSolver's internals to the undo mechanism.

We can solve this problem with the Memento pattern. A memento is an object
that stores a snapshot of the internal state of another object—the memento's
originator. The undo mechanism will request a memento from the originator
when it needs to checkpoint the originator's state. The originator initializes the
memento with informationthat characterizesits current state.Only the originator
can store and retrieve information from the memento—thememento is "opaque"
to other objects.

In the graphical editor example just discussed, the ConstraintSolver can act as an
originator. The following sequence of events characterizes the undo process:

1. The editor requests a memento from the ConstraintSolver as a side-effect of
   the move operation.
2. The ConstraintSolver creates and returns a memento, an instance of a class
   SolverState in this case.A SolverState memento contains data structures that
   describe the current state of the ConstraintSolver's internal equations and
   variables.
3. Later when the user undoes the move operation, the editor gives the SolverState back to theConstraintSolver.
4. Based on the information in the SolverState, the ConstraintSolver changes
   its internal structures to return its equations and variables to their exact
   previous state.

This arrangement lets the ConstraintSolver entrust other objects with the information it needs to revert to a previous
statewithout exposing itsinternal structure
and representations.

---

Use the Memento pattern when you want to produce snapshots of the object’s state to be able to restore a previous state
of the object.

The Memento pattern lets you make full copies of an object’s state, including private fields, and store them separately
from the object. While most people remember this pattern thanks to the “undo” use case, it’s also indispensable when
dealing with transactions (i.e., if you need to roll back an operation on error).

Use the pattern when direct access to the object’s fields/getters/setters violates its encapsulation.

The Memento makes the object itself responsible for creating a snapshot of its state. No other object can read the
snapshot, making the original object’s state data safe and secure.

### Known Uses

- ...

Undo/Redo functionality: Many applications implement undo and redo features by storing snapshots (Mementos) of an
object's state, allowing users to revert to previous states or redo actions.

Checkpoint systems in games: Saving game progress by capturing snapshots of the game state at certain points, allowing
players to return to these states if needed.

Transaction rollback: Database management systems often use the Memento pattern to implement rollback mechanisms by
saving states before transactions and restoring them if needed.

Text editors and document management: Storing historical versions of documents or allowing users to revert to previous
document states.

Configuration management: Capturing and restoring configurations or settings in applications, allowing users to switch
between different configurations.

Collaborative editing: Supporting collaborative work by enabling users to revert changes or view previous versions in
collaborative environments.

Workflow management: Capturing and restoring states in complex workflows or processes, enabling rollback or reversion to
specific points in the workflow.

Caching mechanisms: In caching systems, storing snapshots of data or system states to optimize performance and restore
previous states when needed.

Session management in web applications: Storing and restoring session states for users in web applications, allowing
users to resume their activities after interruptions or logouts.

Drawing and graphic editing applications: Storing states of graphical objects (such as shapes, colors, positions) to
enable undo/redo functionality and revert to previous editing stages.

E-commerce shopping cart: Saving the state of a shopping cart before checkout or during the checkout process to allow
users to revert or resume their shopping session.

Stateful components in user interfaces: Saving and restoring the state of complex UI components (like collapsible
panels, modal dialogs, etc.) when interacting with the user.

Finite state machines: Managing states and transitions in applications with finite state machines by capturing and
restoring the state during different transitions.

Simulation and modeling: Storing and retrieving simulation states in scientific simulations or modeling applications to
analyze different scenarios or revert to previous simulation states.

Inventory systems in games: Saving and restoring inventory states of characters or items in games, allowing players to
go back to previous inventory configurations.

Configuration wizards: Storing states during a step-by-step configuration process, allowing users to revisit or change
previous steps without starting over.

Cryptographic key management: Storing previous states of cryptographic keys or security-related configurations to revert
to a known secure state if needed.

Version control systems: Behind the scenes, version control systems like Git use concepts similar to the Memento pattern
to store and retrieve different versions of code repositories.

Chat or messaging applications: Saving and restoring conversation states, allowing users to view past messages or revert
to previous chat states.

Traffic control systems: Capturing and restoring traffic signal states in smart traffic systems to manage traffic flow
or restore previous patterns.

java.util.Date (the setter methods do that, Date is internally represented by a long value)
All implementations of java.io.Serializable
All implementations of javax.faces.component.StateHolder

The preceding sample code is based on Unidraw's support for connectivity
through its CSolver class [VL901.
Collections in Dylan [App92] provide an iteration interface that reflects the Me-
mento pattern. Dylan's collections have the notion of a "state" object, which is a
memento that represents the state of the iteration. Each collection can represent
the current state of the iteration in any way it chooses; the representation is com-
pletely hidden from clients. The Dylan iteration approach might be translated to
C++ as follows

```text
template <class Item>
class Collection {
public:
Collection();
IterationState* CreatelnitialState();
void Next(IterationState*);
bool IsDone(const IterationState*) const;
Item Currentltem(const IterationState*) const;
IterationState* Copy(const IterationState*) const;
void Append(const Item&);
void Remove(const Item&);
// . . .
};
```

CreatelnitialState returns an initialized IterationState object for the
collection. Next advances the state object to the next position in the iteration;
it effectively increments the iteration index. Is Done returns true if Next has
advanced beyond the last element in the collection. Currentltem dereferences
the state object and returns the element in the collection to which it refers. Copy
returns a copy of the given state object. This is useful for marking a point in an
iteration.
Given a class ItemType, we can iterate over a collection of its instances as
follows7:

```text
class ItemType {
public:
void Process();
// . . .
};
Collection<ItemType*> aCollection;
IterationState* state;
state - aCollection.CreatelnitialState();
while (laCollection.IsDone(state)) {
aCollection.CurrentItern(state)->Process();
aCollection.Next(state);
}
delete state;
```

The memento-based iteration interface has two interesting benefits:

1. More than one state can work on the same collection. (The same is true of the
   Iterator (257) pattern.)
2. . It doesn't require breaking a collection's encapsulation to support iteration.
   The memento is only interpreted by the collection itself; no one else has access
   to it. Other approaches to iteration require breaking encapsulation by making
   iterator classes Mends of their collection classes (see Iterator (257)). The
   situation is reversed in the memento-based implementation: Collection
   is a friend of the iteratorState.
   The QOCA constraint-solving toolkit stores incremental information in memen-
   tos [HHMV92]. Clients can obtain a memento that characterizes the current so-
   lution to a system of constraints. The memento contains only those constraint
   variables that have changed since the last solution. Usually only a small subset
   of the solver's variables changes for each new solution. This subset is enough
   to return the solver to the preceding solution; reverting to earlier solutions re-
   quires restoring mementos from the intervening solutions. Hence you can't set
   mementos in any order; QOCA relies on a history mechanism to revert to earlier
   solutions.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Composition**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- What private information is stored outside an object, and when.

### Solution to causes of redesign

- Dependence on object representations or implementations.
    - Clients that know how an object is represented, stored, located, or implemented might need to be changed when the
      object changes.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

The Memento pattern has several consequences:

1. Preserving encapsulation boundaries. Memento avoids exposing information
   that only an originator should manage but that must be stored nevertheless
   outside the originator. The pattern shields other objects from potentially
   complex Originator internals, thereby preserving encapsulation boundaries.
2. It simplifies Originator. In other encapsulation-preserving designs, Originator
   keeps the versions of internal state that clients have requested. That puts
   all the storage management burden on Originator. Having clients manage
   the state they ask for simplifies Originator and keeps clients from having to
   notify originators when they're done.
3. Using mementos might beexpensive. Mementosmight incur considerable overhead if Originator must copy large amounts of
   information to store in the
   memento or if clients create and return mementos to the originator often
   enough. Unless encapsulating and restoring Originator state is cheap, the
   pattern might not be appropriate. See the discussion ofincrementality in the
   Implementation section.
4. Defining narrow and wide interfaces. It may be difficult in some languages to
   ensure that only the originator can accessthe memento's state.
5. Hidden costs in caring for mementos. A caretakeris responsible for deleting the
   mementos it cares for. However, the caretaker has no idea how much state is n the memento. Hence an otherwise
   lightweight caretaker might incur large
   storage costs when it stores mementos

You can produce snapshots of the object’s state without violating its encapsulation.
You can simplify the originator’s code by letting the caretaker maintain the history of the originator’s state.

The app might consume lots of RAM if clients create mementos too often.
Caretakers should track the originator’s lifecycle to be able to destroy obsolete mementos.
Most dynamic programming languages, such as PHP, Python and JavaScript, can’t guarantee that the state within the
memento stays untouched.

### Relations with Other Patterns
_Distinction from other patterns:_
_Combination with other patterns:_

- ...

Command (233): Commands can use mementos to maintain state for undoable
operations.
Iterator (257): Mementos can be used for iteration as described earlier.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

(recognizeable by behavioral methods which internally changes the state of the whole instance)

### Structure

```mermaid
classDiagram
    class Originator {
        - state: string
        + createMemento(): Memento
        + setMemento(memento: Memento): void
    }
    class Memento {
        - state: string
        + getState(): string
        + setState(state: string): void
    }
    class Caretaker {
        - mementos: Memento[]
        + addMemento(memento: Memento): void
        + getMemento(index: number): Memento
    }

    Originator --> Memento: instantiates
    Caretaker o-- Memento: aggregated by
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

Here are two issues to consider when implementing the Memento pattern:

1. Language support. Mementos have two interfaces: a wide one for originators
   and a narrow one for other objects. Ideally the implementation language
   will support two levels of static protection. C++ lets you do this by making
   the Originator a friend of Memento and making Memento's wide interface
   private. Only the narrow interface should be declared public. For example:

```text
class State;
class Originator {
public:
Memento* CreateMemento();
void SetMemento(const Memento*);
// . . .
private:
State* _state; // internal data structures
// . . .
};
class Memento {
public:
// narrow public interface
virtual ~Memento();
private:
// private members accessible only to Originator
friend class Originator;
Memento();
void SetState(State*);
State* GetStateO ;
// . . .
private:
State* _state;
// . - .
};
```

2. Storing incremental changes. When mementos get created and passed back to
   their originator in a predictable sequence, then Memento can save just the
   incremental change to the originator's internal state.
   For example, undoable commands in a history list can use mementos to en-
   sure that commands are restored to their exact state when they're undone
   (see Command (233)). The history list defines a specific order in which com-
   mands can be undone and redone. That means mementos can store just the
   incremental change that a command makes rather than the full state of every
   object they affect. In the Motivation example given earlier, the constraint
   solver can store only those internal structures that change to keep the line connecting the rectangles, as opposed to
   storing the absolute positions of
   these objects.

### Implementation

https://www.youtube.com/watch?v=HhZ4DtON404
In the example we apply the memento pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Memento](https://refactoring.guru/design-patterns/memento)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Milan Jovanovic: Domain-Driven Design Without An ORM Using The Snapshot Pattern](https://youtu.be/HhZ4DtON404?si=qdv4KA6ELKmtzs7u)
- [Geekific: The Memento Pattern Explained and Implemented in Java](https://youtu.be/_Q5rXfGuyLQ?si=8yeLm3pwkLX6vfYw)

<br>
<br>
